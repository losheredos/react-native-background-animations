
# React Native Background Animations Library

A React Native library for adding beautiful background animations to your mobile apps. This library provides a collection of customizable components to create stunning(hopefully) visual effects that enhance user experience.

## Installation

To get started with the library, you can install it using npm or yarn:

```bash
npm install react-native-background-animations

yarn add react-native-background-animations
```

```bash
import React from 'react';
import { View } from 'react-native';

import {StarryNight, RainyBackground, CloudyBackground} from 'react-native-background-animations';

function App() {
  return (
    <View style={{ flex: 1 }}>
      <StarryNight>
      {/* Your other components */}
      </StarryNight>
      
      <RainyBackground>
      {/* Your other components */}
      </RainyBackground>
      
      <CloudyBackground>
      {/* Your other components */}
      </CloudyBackground>
    </View>
  );
}

export default App;

```
## Props

```bash
<StarryNightBackground ... />
```
| Prop       | Type     | Default | Required   | Description                         |
|------------|----------|----------|-----------|-------------------------------------|
| `minScale`    | number   | 1.00      |     No     | Minimum value to animate in scaling of the stars.        |
| `maxScale`    | number   | 1.25       | No         | Maximum value to animate in scaling of the stars.       |
| `starSize`    | number  | 8       | No   | Size of the each star.      |
| `starAmount`    | number  | 16       | No   | Total amount of stars which will be used in animation.      |
| `animationDuration`    | number  | 3000       | No   | Total duration of the animation as milliseconds.      |
| `transitionDelay`    | number  | 500       | No   | Transition delay before the fade-out animation.      |
| `shootingStars`    | boolean  | false       | No   | When enabled each star each star will move towards random direction before the fading out.      |
| `containerStyle`    | ViewStyle   |       |     No     | Styles of the container.        |
| `children`    | JSX.Element[]   |       |     No     | Styles of the container.        |

![Starry-Plain](https://i.ibb.co/HF4tpD3/Simulator-Screen-Recording-i-Phone-14-2023-09-19-at-12-13-13.gif)

```bash
<RainyBackground ... />
```
| Prop       | Type     | Default | Required   | Description                         |
|------------|----------|----------|-----------|-------------------------------------|
| `dropSize`    | number   | 30      |     No     | Size of each rain drop.        |
| `dropAmount`    | number   | 32      |     No     | Total amount of drops in the animation.        |
| `animationDuration`    | number   | 1000      |     No     | Total duration of the animation as milliseconds. |
| `windEffectRate`    | number   | 0      |     No     | Rate of wind effect in the animation. |
| `minDelay`    | number   |    500   |     No     | Minimum possible delay before the start of the animation of each drop.        |
| `maxDelay`    | number   | 5000    |     No     | Maximum possible delay before the start of the animation of each drop.   |
| `containerStyle`    | ViewStyle   |       |     No     | Styles of the container.        |
| `children`    | JSX.Element[]   |       |     No     | Styles of the container.        |

![Starry-Plain](https://i.ibb.co/BKjg3HH/Simulator-Screen-Recording-i-Phone-14-2023-09-19-at-12-14-13-1.gif)

```bash
<CloudyBackground ... />
```
| Prop       | Type     | Default | Required   | Description                         |
|------------|----------|----------|-----------|-------------------------------------|
| `cloudSize`    | number   | 50      |     No     | Size of the each cloud.        |
| `cloudSet`    | number   | 4      |     No     | Total amount of cloud set the be animated.        |
| `cloudAmountInSet`    | number   | 3      |     No     | Total amount of the clouds in each set. |
| `animationDuration`    | number   | 3000      |     No     |  Total duration of the animation as milliseconds. |
| `animationDelay`    | number   |    0.5 * animationDuration   |     No     | Animation delay before the start of each animation.        |
| `containerStyle`    | ViewStyle   |       |     No     | Styles of the container.        |
| `children`    | JSX.Element[]   |       |     No     | Styles of the container.        |

