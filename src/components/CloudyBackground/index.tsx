import React, {useEffect, useRef} from 'react';
import {Animated, View, ViewStyle} from 'react-native';

import {FULL_SH, FULL_SW} from '../../utils';
import {getRandomFloat} from '../../utils/helperFunctions';

import styles from './styles';

interface Props {
    cloudSize?: number;
    cloudSet?: number;
    cloudAmountInSet?: number;
    animationDuration?: number;
    animationDelay?: number;
    containerStyle?: ViewStyle;
    children?: JSX.Element | JSX.Element[];
}

const transY_min_1 = 0;
const transY_max_1 = FULL_SH * 0.3;
const transY_min_2 = FULL_SH * 0.33;
const transY_max_2 = FULL_SH * 0.66;
const transY_min_3 = FULL_SH * 0.69;
const transY_max_3 = FULL_SH * 0.92;

const transX_min = FULL_SW * 0.1;
const transX_max = FULL_SW * 0.9;

const CloudyBackground = ({
    cloudSize = 50,
    cloudSet = 4,
    cloudAmountInSet = 3,
    animationDuration = 3000,
    animationDelay = animationDuration / 2,
    containerStyle,
    children,
}: Props) => {
    function getCloudValues() {
        const cloudValuesList = [];
        for (let i = 0; i < cloudSet; i++) {
            const eachSetList = [];
            for (let k = 0; k < cloudAmountInSet; k++) {
                eachSetList.push({
                    opacity: new Animated.Value(0),
                    translateX: new Animated.Value(0),
                    translateY: new Animated.Value(0),
                });
            }
            cloudValuesList.push(eachSetList);
        }
        return cloudValuesList;
    }

    const cloudValues = useRef(getCloudValues()).current;

    useEffect(() => {
        for (let i = 0; i < cloudSet; i++) {
            animateCloudSet(i);
        }
    }, []);

    function animateCloudSet(index: number) {
        for (let i = 0; i < cloudAmountInSet; i++) {
            animateCloud(index, i);
        }
    }

    function animateCloud(index: number, innerIndex: number) {
        const translateX = getRandomFloat(transX_min, transX_max);
        let startY = 0,
            startX;
        switch (index % 3) {
            case 0:
                {
                    startY = getRandomFloat(transY_min_1, transY_max_1);
                }
                break;
            case 1:
                {
                    startY = getRandomFloat(transY_min_2, transY_max_2);
                }
                break;
            case 2:
                {
                    startY = getRandomFloat(transY_min_3, transY_max_3);
                }
                break;
        }
        switch (innerIndex) {
            case 0:
                {
                    startX = getRandomFloat(FULL_SW * 0.1, FULL_SW * 0.48);
                }
                break;
            default: {
                startX = getRandomFloat(FULL_SW * 0.5, FULL_SW * 0.98);
                if (innerIndex == 2) {
                    startX *= 1.05;
                }
            }
        }

        const currentVal = cloudValues[index][innerIndex];
        currentVal.translateY.setValue(startY);
        currentVal.translateX.setValue(startX);

        Animated.sequence([
            Animated.delay(
                getRandomFloat(animationDelay, animationDelay * 1.5),
            ),
            Animated.parallel([
                Animated.timing(currentVal.opacity, {
                    toValue: 1,
                    duration: getRandomFloat(
                        animationDuration / 3,
                        animationDuration / 2,
                    ),
                    useNativeDriver: true,
                }),
                Animated.timing(currentVal.translateX, {
                    toValue: translateX - translateX * 0.05 * innerIndex,
                    duration: animationDuration,
                    useNativeDriver: true,
                }),
            ]),
            Animated.timing(currentVal.opacity, {
                toValue: 0,
                duration: getRandomFloat(
                    animationDuration / 3,
                    animationDuration / 2,
                ),
                useNativeDriver: true,
            }),
        ]).start(() => {
            animateCloud(index, innerIndex);
        });
    }

    function getClouds() {
        const clouds = [];
        for (let i = 0; i < cloudSet; i++) {
            for (let k = 0; k < cloudAmountInSet; k++) {
                const valuesOfCloud = cloudValues[i][k];
                clouds.push(
                    <Animated.Image
                        source={require('./cloud.png')}
                        key={`${i} ${k}`}
                        style={{
                            position: 'absolute',
                            zIndex: k,
                            opacity: valuesOfCloud?.opacity,
                            width: cloudSize,
                            height: cloudSize,
                            transform: [
                                {
                                    translateX: valuesOfCloud?.translateX,
                                },
                                {
                                    translateY: valuesOfCloud?.translateY,
                                },
                            ],
                        }}
                    />,
                );
            }
        }
        return clouds;
    }

    return (
        <View style={containerStyle ? containerStyle : styles.container}>
            {children}
            {getClouds()}
        </View>
    );
};

export default CloudyBackground;
