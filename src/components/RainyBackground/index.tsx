import React, {useEffect, useRef} from 'react';
import {Animated, View, ViewStyle} from 'react-native';

import {FULL_SH, FULL_SW, helperFunctions} from '../../utils';

import styles from './styles';

interface Props {
    dropSize?: number;
    dropAmount?: number;
    animationDuration?: number;
    windEffectRate?: number;
    minDelay?: number;
    maxDelay?: number;
    children?: JSX.Element | JSX.Element[];
    containerStyle?: ViewStyle;
}
const defaultScale = 0.5;
const transX_min_1 = 0;
const transX_max_1 = FULL_SW * 0.23;
const transX_min_2 = FULL_SW * 0.25;
const transX_max_2 = FULL_SW * 0.48;

const transX_min_3 = FULL_SW * 0.5;
const transX_max_3 = FULL_SW * 0.78;
const transX_min_4 = FULL_SW * 0.8;
const transX_max_4 = FULL_SW;

const RainyBackground = ({
    dropSize = 30,
    dropAmount = 32,
    animationDuration = 1000,
    windEffectRate = 0,
    minDelay = 500,
    maxDelay = 5000,
    containerStyle,
    children,
}: Props) => {
    function getRainValues() {
        const starValuesList = [];
        for (let i = 0; i < dropAmount; i++) {
            starValuesList.push({
                opacity: new Animated.Value(0),
                scale: new Animated.Value(defaultScale),
                translateX: new Animated.Value(0),
                translateY: new Animated.Value(0),
            });
        }
        return starValuesList;
    }

    const rainValues: any = useRef(getRainValues()).current;

    useEffect(() => {
        for (let i = 0; i < dropAmount; i++) {
            animateRainDrop(i);
        }
    }, []);

    function getWindEffect(i: number, newTranslateX: number) {
        if (windEffectRate !== 0) {
            return Animated.timing(rainValues[i].translateX, {
                toValue: newTranslateX + windEffectRate * 10,
                duration: animationDuration * 0.9,
                useNativeDriver: true,
            });
        }
    }

    function animateRainDrop(i: number) {
        let maxTranslateX,
            minTranslateX,
            maxTranslateY = FULL_SH * 1.2;

        switch (i % 4) {
            case 0:
                {
                    minTranslateX = transX_min_1;
                    maxTranslateX = transX_max_1;
                }
                break;
            case 1:
                {
                    minTranslateX = transX_min_2;
                    maxTranslateX = transX_max_2;
                }
                break;
            case 2:
                {
                    minTranslateX = transX_min_3;
                    maxTranslateX = transX_max_3;
                }
                break;
            case 3:
                {
                    minTranslateX = transX_min_4;
                    maxTranslateX = transX_max_4;
                }
                break;
        }
        const newTranslateX = helperFunctions.getRandomFloat(
            minTranslateX,
            maxTranslateX,
        );
        const newOpacity = helperFunctions.getRandomFloat(0.5, 1);

        rainValues[i].translateX.setValue(newTranslateX);

        Animated.sequence([
            Animated.delay(helperFunctions.getRandomNumber(minDelay, maxDelay)),
            Animated.parallel([
                Animated.timing(rainValues[i].translateY, {
                    toValue: maxTranslateY,
                    duration: animationDuration,
                    useNativeDriver: true,
                }),
                Animated.timing(rainValues[i].opacity, {
                    toValue: newOpacity,
                    duration: animationDuration * 0.5,
                    useNativeDriver: true,
                }),
                getWindEffect(i, newTranslateX),
            ]),
        ]).start(() => {
            rainValues[i].opacity.setValue(0);
            rainValues[i].translateY.setValue(0);
            animateRainDrop(i);
        });
    }

    function getRainDrops() {
        const stars = [];
        for (let i = 0; i < dropAmount; i++) {
            const valuesOfDrop = rainValues[i];
            stars.push(
                <Animated.View
                    key={i}
                    style={{
                        position: 'absolute',
                        opacity: valuesOfDrop?.opacity,
                        backgroundColor: 'white',
                        width: dropSize / 7,
                        height: dropSize,
                        transform: [
                            {
                                translateX: valuesOfDrop?.translateX,
                            },
                            {
                                translateY: valuesOfDrop?.translateY,
                            },
                            {
                                scale: valuesOfDrop?.scale,
                            },
                        ],
                    }}
                />,
            );
        }
        return stars;
    }

    return (
        <View style={containerStyle ? containerStyle : styles.container}>
            {children}
            {getRainDrops()}
        </View>
    );
};

export default RainyBackground;
