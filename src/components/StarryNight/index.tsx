import React, {useEffect, useRef} from 'react';
import {Animated, View, ViewStyle} from 'react-native';

import {SW, SH, FULL_SH, FULL_SW, helperFunctions} from '../../utils';
import styles from './styles';

import Value = Animated.Value;

interface Props {
    children?: JSX.Element | JSX.Element[];
    minScale?: number;
    maxScale?: number;
    starSize?: number;
    animationDuration?: number;
    transitionDelay?: number;
    starAmount?: number;
    shootingStars?: boolean;
    containerStyle?: ViewStyle;
}

const defaultScale = 0.01;
const transX_min_1 = FULL_SW * 0.1;
const transX_max_1 = FULL_SW * 0.28;
const transX_min_2 = FULL_SW * 0.3;
const transX_max_2 = FULL_SW * 0.5;

const transX_min_3 = FULL_SW * 0.52;
const transX_max_3 = FULL_SW * 0.78;
const transX_min_4 = FULL_SW * 0.8;
const transX_max_4 = FULL_SW;

const transY_min_1 = FULL_SH * 0.05;
const transY_max_1 = FULL_SH * 0.48;
const transY_min_2 = FULL_SH * 0.5;
const transY_max_2 = FULL_SH;

const StarryNightBackground = ({
    children,
    minScale = 1,
    maxScale = 1.25,
    starSize = 8,
    starAmount = 16,
    animationDuration = 3000,
    transitionDelay = 500,
    shootingStars = false,
    containerStyle,
}: Props) => {
    function getStarValues() {
        const starValuesList = [];
        for (let i = 0; i < starAmount; i++) {
            starValuesList.push({
                opacity: new Animated.Value(0),
                scale: new Animated.Value(defaultScale),
                translateX: new Animated.Value(0),
                translateY: new Animated.Value(0),
            });
        }
        return starValuesList;
    }

    const starValues: any = useRef(getStarValues()).current;

    useEffect(() => {
        for (let i = 0; i < starAmount; i++) {
            animateStar(i);
        }
    }, []);

    function firstPhaseAnimations(
        newTranslateX: number,
        newTranslateY: number,
        newOpacity: number,
        newScale: number,
        newStarValues: any,
    ) {
        const animations = [
            Animated.timing(newStarValues.opacity, {
                toValue: newOpacity,
                duration: animationDuration,
                useNativeDriver: false,
            }),
            Animated.timing(newStarValues.scale, {
                toValue: newScale,
                duration: animationDuration,
                useNativeDriver: false,
            }),
        ];

        return animations;
    }

    function secondPhaseAnimations(
        shootingTranslateX: number,
        shootingTranslateY: number,
        newStarValues: any,
    ) {
        const animations = [
            Animated.timing(newStarValues.opacity, {
                toValue: 0,
                duration: animationDuration / 2,
                useNativeDriver: false,
            }),
            Animated.timing(newStarValues.scale, {
                toValue: defaultScale,
                duration: animationDuration / 2,
                useNativeDriver: false,
            }),
        ];
        shootingStarAnimation(
            newStarValues,
            shootingTranslateX,
            shootingTranslateY,
        ).forEach(item => animations.push(item));

        return animations;
    }

    function shootingStarAnimation(
        starValues: {
            translateY: Value;
            translateX: Value;
        },
        shootingTranslateX: number,
        shootingTranslateY: number,
    ) {
        if (shootingStars) {
            return [
                Animated.timing(starValues.translateY, {
                    toValue: shootingTranslateY,
                    duration: animationDuration,
                    useNativeDriver: false,
                }),
                Animated.timing(starValues.translateX, {
                    toValue: shootingTranslateX,
                    duration: animationDuration,
                    useNativeDriver: false,
                }),
            ];
        }
        return [];
    }

    function animateStar(index: number) {
        let maxTranslateX, minTranslateX, maxTranslateY, minTranslateY;

        switch (index % 8) {
            case 0:
                {
                    minTranslateX = transX_min_1;
                    maxTranslateX = transX_max_1;

                    minTranslateY = transY_min_1;
                    maxTranslateY = transY_max_1;
                }
                break;
            case 1:
                {
                    minTranslateX = transX_min_2;
                    maxTranslateX = transX_max_2;

                    minTranslateY = transY_min_1;
                    maxTranslateY = transY_max_1;
                }
                break;
            case 2:
                {
                    minTranslateX = transX_min_3;
                    maxTranslateX = transX_max_3;

                    minTranslateY = transY_min_1;
                    maxTranslateY = transY_max_1;
                }
                break;
            case 3:
                {
                    minTranslateX = transX_min_4;
                    maxTranslateX = transX_max_4;

                    minTranslateY = transY_min_1;
                    maxTranslateY = transY_max_1;
                }
                break;

            case 4:
                {
                    minTranslateX = transX_min_1;
                    maxTranslateX = transX_max_1;

                    minTranslateY = transY_min_2;
                    maxTranslateY = transY_max_2;
                }
                break;
            case 5:
                {
                    minTranslateX = transX_min_2;
                    maxTranslateX = transX_max_2;

                    minTranslateY = transY_min_2;
                    maxTranslateY = transY_max_2;
                }
                break;
            case 6:
                {
                    minTranslateX = transX_min_3;
                    maxTranslateX = transX_max_3;

                    minTranslateY = transY_min_2;
                    maxTranslateY = transY_max_2;
                }
                break;
            case 7:
                {
                    minTranslateX = transX_min_4;
                    maxTranslateX = transX_max_4;

                    minTranslateY = transY_min_2;
                    maxTranslateY = transY_max_2;
                }
                break;
        }

        const newScale = helperFunctions.getRandomFloat(minScale, maxScale);
        const newTranslateX = helperFunctions.getRandomFloat(
            minTranslateX,
            maxTranslateX,
        );
        const newTranslateY = helperFunctions.getRandomFloat(
            minTranslateY,
            maxTranslateY,
        );
        const newOpacity = helperFunctions.getRandomFloat(0.8, 1);

        const newValues = {...starValues[index]};

        newValues.translateX.setValue(newTranslateX);
        newValues.translateY.setValue(newTranslateY);

        const shootingTranslateX = helperFunctions.getRandomNumber(
            newTranslateX * 0.2,
            newTranslateX * 1.5,
        );
        const shootingTranslateY = helperFunctions.getRandomNumber(
            newTranslateY * 0.5,
            newTranslateY * 1.5,
        );

        Animated.sequence([
            Animated.delay(helperFunctions.getRandomNumber(250, 5000)),
            Animated.parallel(
                firstPhaseAnimations(
                    newTranslateX,
                    newTranslateY,
                    newOpacity,
                    newScale,
                    newValues,
                ),
            ),
            Animated.delay(transitionDelay),
            Animated.parallel(
                secondPhaseAnimations(
                    shootingTranslateX,
                    shootingTranslateY,
                    newValues,
                ),
            ),
        ]).start(() => {
            newValues.translateX.setValue(0);
            newValues.translateY.setValue(0);
            newValues.scale.setValue(0);

            starValues[index] = newValues;

            animateStar(index);
        });
    }

    function getStars() {
        const stars = [];
        for (let i = 0; i < starAmount; i++) {
            const valuesOfStar = starValues[i];
            stars.push(
                <Animated.Image
                    key={i}
                    source={require('./star.png')}
                    style={{
                        position: 'absolute',
                        opacity: valuesOfStar?.opacity,
                        width: starSize,
                        height: starSize,
                        transform: [
                            {
                                translateX: valuesOfStar?.translateX,
                            },
                            {
                                translateY: valuesOfStar?.translateY,
                            },
                            {
                                scale: valuesOfStar?.scale,
                            },
                        ],
                    }}
                />,
            );
        }
        return stars;
    }

    return (
        <View style={containerStyle ? containerStyle : styles.container}>
            {getStars()}
            {children}
        </View>
    );
};

export default StarryNightBackground;
