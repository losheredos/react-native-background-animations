export function getRandomNumber(start = 1, end = 100){
    return Math.floor(Math.random() * end) + start
}

export function getRandomFloat(min: number = 1, max: number = 10, decimals = 2) {
    const str = (Math.random() * (max - min) + min).toFixed(decimals);
  
    return parseFloat(str);
  }
  