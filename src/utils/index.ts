import {Dimensions} from 'react-native';

import * as helperFunctions from './helperFunctions';

const FULL_SW = Dimensions.get('window').width;
const FULL_SH = Dimensions.get('window').height;

const SW = FULL_SW / 100;
const SH = FULL_SH / 100;

export {FULL_SH, FULL_SW, SW, SH, helperFunctions};
